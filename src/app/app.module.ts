import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { UserData } from '../providers/user-data';
import { SignupPage } from '../pages/signup/signup';
import { LoginPage } from '../pages/login/login';
import { AccountPage } from '../pages/account/account';
import { SearchPage } from '../pages/search/search';
import { DetailsPage } from '../pages/details/details';
import { IonicStorageModule } from '@ionic/storage';
import { StartPage } from "../pages/start/start";
import { HttpModule } from '@angular/http';

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule, AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { ExperienceServiceProvider } from '../providers/experience-service';
import { SubscribePage} from "../pages/subscribe/subscribe";

export const firebaseConfig = {
    apiKey: "",
    authDomain: "",
    databaseURL: "https://hackathon-3abc3.firebaseio.com",
    storageBucket: "",
    messagingSenderId: ""
};

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    SignupPage,
    AccountPage,
    SearchPage,
    DetailsPage,
    StartPage,
      SubscribePage
  ],
  imports: [
    HttpModule,
    BrowserModule,
      IonicModule.forRoot(MyApp, {
          swipeBackEnabled: true,
          backButtonText: '',
          iconMode: 'md',
          modalEnter: 'md-transition',
          modalLeave: 'md-transition',
          tabsPlacement: 'top',
          scrollAssist: false
      }, {
          links: [
              { component: LoginPage, name: 'LoginPage', segment: 'login' },
              { component: AccountPage, name: 'AccountPage', segment: 'account' },
              { component: SignupPage, name: 'SignupPage', segment: 'signup' },
              { component: SearchPage, name: 'SearchPage', segment: 'search' },
              { component: DetailsPage, name: 'DetailsPage', segment: 'details' },
              { component: StartPage, name: 'StartPage', segment: 'start' }
          ]
      }),
      IonicStorageModule.forRoot(),
      AngularFireModule.initializeApp(firebaseConfig),
      AngularFireDatabaseModule,
      AngularFireAuthModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    SignupPage,
    AccountPage,
    SearchPage,
    DetailsPage,
    StartPage,
      SubscribePage
  ],
  providers: [
    AngularFireDatabase,
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    UserData,
    Storage,
    ExperienceServiceProvider
  ]
})
export class AppModule {}
