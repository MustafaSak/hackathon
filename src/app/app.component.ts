import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { StartPage } from "../pages/start/start";
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = HomePage;

  constructor(public platform: Platform,
              public statusBar: StatusBar,
              public splashScreen: SplashScreen) {

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
        if(!this.platform.is('core')) {
            statusBar.styleDefault();
            splashScreen.hide();
        }
        if(localStorage.getItem('resgistred') != undefined) {
            this.rootPage = HomePage;
        } else {
            this.rootPage = StartPage;
        }

    });
  }
}

