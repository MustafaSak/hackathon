import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { AngularFireDatabase } from "angularfire2/database";

/*
  Generated class for the ExperienceServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ExperienceServiceProvider {
  public experience: any;
  public search: any;
  public product: any;
  public detail: any;

  constructor(public http: Http,
              public db: AngularFireDatabase) {

  }

  getExperience(filter: string) {
      return this.experience = this.db.list('/experience/' + filter ).valueChanges();
  }

    getSearch(filter: string) {
        return this.search = this.db.list('/experience/' + filter ).valueChanges();
    }

    getProduct(filter: string, id: string) {

        return this.product = this.db.object('/experience/' + filter + '/'+ id )
              .snapshotChanges()
              .subscribe(action => {
                  this.detail = action.payload.val();
                  console.log(this.detail);
                  console.log(this.detail['participants']['02'])

              });
    }

}
