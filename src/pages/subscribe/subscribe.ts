import { Component } from '@angular/core';
import {IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';
import {HomePage} from "../home/home";

/**
 * Generated class for the SubscribePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-subscribe',
  templateUrl: 'subscribe.html',
})
export class SubscribePage {

  public vue: string = null;
  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public viewCtrl: ViewController) {
    this.navParams.get('vue') != undefined ? this.vue = this.navParams.get('vue') :'';
    console.log(this.vue);
  }

  dismiss(value){
    if(value) {
      this.navCtrl.setRoot(HomePage);
      this.viewCtrl.dismiss();
    } else {
      this.viewCtrl.dismiss();
    }

  }

  goPaiement() {
    this.vue = 'paiement';
  }

}
