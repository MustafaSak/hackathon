import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { NgForm } from '@angular/forms';
import { UserData } from '../../providers/user-data';
import { UserOptions } from '../../interfaces/user-options';
//import { TabsPage } from '../tabs-page/tabs-page';

/**
 * Generated class for the SignupPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {
    signup: UserOptions = { username: '', password: '' };
    submitted = false;

    constructor(public navCtrl: NavController,
                public userData: UserData) {}

    onSignup(form: NgForm) {
        this.submitted = true;

        if (form.valid) {
            localStorage.setItem('user', this.signup.username);
            localStorage.setItem('resgistred', 'true');
            this.userData.signup(this.signup.username);
            //this.navCtrl.push(TabsPage);
        }
    }
}
