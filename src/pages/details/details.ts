import {Component, ViewChild} from '@angular/core';
import {ModalController, NavController, NavParams} from 'ionic-angular';
import { Slides } from 'ionic-angular';
import { Subscription } from "rxjs/Subscription";
import { ExperienceServiceProvider } from "../../providers/experience-service";
import {SubscribePage} from "../subscribe/subscribe";

/**
 * Generated class for the DetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-details',
  templateUrl: 'details.html',
})
export class DetailsPage {
    public id: string;
    private product: any;
    private product_detail = [];
    private members = [];

    @ViewChild(Slides) slides: Slides;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public expService: ExperienceServiceProvider,
              public modalCtrl: ModalController) {

    this.id = this.navParams.get("id");
    this.getProduct("populaire",this.id);

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailsPage');
  }

    getProduct(filter: string, id: string) {
        this.expService.getProduct(filter, id);
        console.log(this.expService.detail);
    }

    subscribe() {
        const myModal = this.modalCtrl.create(SubscribePage, {
            vue: 'pay'
        });

        myModal.present();
    }

}
