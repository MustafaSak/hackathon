import { Component } from '@angular/core';
import { AlertController, NavController, NavParams } from 'ionic-angular';
import { UserData } from '../../providers/user-data';
import { DetailsPage } from "../details/details";

/**
 * Generated class for the AccountPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-account',
  templateUrl: 'account.html',
})
export class AccountPage {
    username: string;

    constructor(public alertCtrl: AlertController,
                public navCtrl: NavController,
                public userData: UserData) {
                console.log(this.username);

                this.username = localStorage.getItem('user');

    }

    ngAfterViewInit() {
        this.getUsername();
    }

    updatePicture() {
        console.log('Clicked to update picture');
    }

    // Present an alert with the current username populated
    // clicking OK will update the username and display it
    // clicking Cancel will close the alert and do nothing
    changeUsername() {
        let alert = this.alertCtrl.create({
            title: 'Change Username',
            buttons: [
                'Cancel'
            ]
        });
        alert.addInput({
            name: 'username',
            value: this.username,
            placeholder: 'username'
        });
        alert.addButton({
            text: 'Ok',
            handler: (data: any) => {
                this.userData.setUsername(data.username);
                this.getUsername();
            }
        });

        alert.present();
    }

    getUsername() {
        this.userData.getUsername().then((username) => {
            this.username = username;
        });
    }

    changePassword() {
        console.log('Clicked to change password');
    }

    logout() {
        this.userData.logout();
        this.navCtrl.setRoot('LoginPage');
    }

    support() {
        this.navCtrl.push('SupportPage');
    }

    details() {
        this.navCtrl.push(DetailsPage);
    }
}
