import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { LoginPage } from "../login/login";
import { SearchPage } from "../search/search";
import { DetailsPage } from "../details/details";
import { AccountPage } from "../account/account";
import { ExperienceServiceProvider } from "../../providers/experience-service";
import { Subscription } from "rxjs/Subscription";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
    private filter: string;
    private experience: Subscription;
    private key: any;

  constructor(public navCtrl: NavController,
              public expService: ExperienceServiceProvider) {

    this.getExperience('populaire');
    this.filter = "populaire";
  }

  login() {
    this.navCtrl.push(LoginPage);
  }

  search() {
      this.navCtrl.push(SearchPage);
  }

  details(id: string) {
    this.navCtrl.push(DetailsPage, {
        id : id
    });
  }

  account() {
    this.navCtrl.push(AccountPage);
  }

  getExperience(filter: string) {
      this.experience = this.expService.getExperience(filter).subscribe(response => {
        console.log(response);
      });
      this.experience.unsubscribe();
  }


  segmentChanged(event) {
    this.getExperience(event.value);
  }
}
