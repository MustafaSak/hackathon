import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {Subscription} from "rxjs/Subscription";
import {ExperienceServiceProvider} from "../../providers/experience-service";
import {DetailsPage} from "../details/details";

/**
 * Generated class for the SearchPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-search',
  templateUrl: 'search.html',
})
export class SearchPage {
    private experience: Subscription;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public expService: ExperienceServiceProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SearchPage');
  }
    details(id: string) {
        this.navCtrl.push(DetailsPage, {
            id : id
        });
    }

    onInput() {
        this.experience = this.expService.getSearch('populaire').subscribe(response => {
            console.log(response);
        });
        this.experience.unsubscribe();
    }

}
