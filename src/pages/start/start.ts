import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { HomePage } from "../home/home";
import { LoginPage } from "../login/login";

/**
 * Generated class for the StartPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-start',
  templateUrl: 'start.html',
})
export class StartPage {
  public step: number;
  public user: FormGroup;
  public account: FormGroup;
  public preference: FormGroup;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public formBuilder: FormBuilder) {

    this.initForms();
    this.step = 1;
  }

  initForms() {
      this.user = this.formBuilder.group({
          name: ['', Validators.required]
      });

      this.account = this.formBuilder.group({
          email: ['', Validators.email],
          password : ['', Validators.compose([
              Validators.required,
              Validators.minLength(6),
              Validators.maxLength(32)])],
          confirmpassword : ['', Validators.required],
      });

      this.preference = this.formBuilder.group({
          email: ['', Validators.email],
          password : ['', Validators.compose([
              Validators.required,
              Validators.minLength(6),
              Validators.maxLength(32)])],
          confirmpassword : ['', Validators.required],
      });
  }
  nextStep() {
    this.step += 1;
  }

  getPrev(){
      console.log('ok');
      this.step = this.step - 1;
  }

  setUser() {
      localStorage.setItem('user', this.user['name']);
      this.nextStep();
  }

  setAccount() {
    this.nextStep();
  }

  login() {
      this.navCtrl.setRoot(LoginPage);
  }

  goHome() {
      localStorage.setItem('resgistred', 'true');
      this.navCtrl.setRoot(HomePage);
  }
}
