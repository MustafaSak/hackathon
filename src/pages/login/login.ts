import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { UserData } from '../../providers/user-data';
import { NgForm } from '@angular/forms';
import { UserOptions } from '../../interfaces/user-options';
//import { TabsPage } from '../tabs-page/tabs-page';
import { SignupPage } from '../signup/signup';
import { AccountPage } from '../account/account';


/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
    login: UserOptions = { username: '', password: '' };
    submitted = false;

    constructor(public navCtrl: NavController,
                public userData: UserData) { }

    onLogin(form: NgForm) {
        console.log(form);
        this.submitted = true;

        if (form.valid) {
            this.userData.login(this.login.username);
            localStorage.setItem('user', this.login.username);
            this.navCtrl.push(AccountPage);
            console.log(this.login.username);
            //this.navCtrl.push(TabsPage);
        }
    }

    onSignup() {
        this.navCtrl.push(SignupPage);
    }
}
